Para ejecutar.

El archivo que combina las pruebas de ejecución se llama puntoFlotante128Test1.asm. Este debe ser ejecutado con el Mars
desde el mismo directorio donde reside:

[src]$ Mars

Al ejecutar el Mars se produce un fichero de datos llamados Result.dat

Este se prueba con el programa testFP128 (se obtiene de github: 
git@github.com:jfcmacro/testFP128.git)

y se ejecuta testFP128 Result.dat.

Todas las pruebas pasaron.

