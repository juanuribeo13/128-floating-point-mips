.data
maskSll:			.word	0x00000080
maskSrl:			.word	0x00000001
maskExponent:	.word	0x7fff0000
maskSignExponent:	.word	0xffff0000
maskSign:			.word	0x80000000
infinite:			.word	0x00000000,0x00000000,0x00000000,0x7fff0000
nAn:				.word	0x00000000,0x00000000,0x00000000,0x7fff1000
.text
########################################################
#void:addToExponent128(*floatingPointNumber, value)
########################################################
addToExponent128:
	add		$sp, $sp, -12
	sw		$s0, 0($sp)
	sw		$s1, 4($sp)
	sw		$s7, 8($sp)
	lw		$s0, 12($a0)
	la		$s7, maskExponent
	lw		$s7, 0($s7)
	not		$s7, $s7
	and		$s1, $s0, $s7		#Save the exponent and the mantissa
	not		$s7, $s7
	and		$s0, $s0, $s7		#Removing the sign and mantissa
	srl		$s0, $s0, 16
	add		$s0, $s0, $a1
	sll		$s0, $s0, 16
	or		$s0, $s0, $s1		#Restore the sign and mantissa
	sw		$s0, 12($a0)
	lw		$s7, 8($sp)
	lw		$s1, 4($sp)
	lw		$s0, 0($sp)
	add		$sp, $sp, 12
	jr		$ra
########################################################
#void:sll128(*floatingPointNumber, numTimes)
########################################################
sll128:
	add		$sp, $sp, -36
	sw		$s0, 0($sp)
	sw		$s1, 4($sp)
	sw		$s2, 8($sp)
	sw		$s3, 12($sp)
	sw		$s4, 16($sp)
	sw		$s5, 20($sp)
	sw		$s6, 24($sp)
	sw		$s7, 28($sp)
	sw		$ra, 32($sp)
	and		$s0, $s0, $zero	#$s0(i) <- 0
	la		$s7, maskSll
	lw		$s7, 0($s7)		#$s7(mask) <- 0x00000080
While1Sll:
	beq		$s0, $a1, EndWhile1Sll
	and		$s1, $s1, $zero	#$s1(carrey) <- 0
	and		$s2, $s2, $zero	#$s2(j) <- 0
	li		$s3, 14
While2Sll:
	beq		$s2,$s3, EndWhile2Sll
	add		$s4, $s2, $a0
	lbu		$s5, 0($s4)		#$s5 <- number[j]
	and		$s6, $s5,$s7		#tmpCarry <- number[j] & mask
	sll		$s5, $s5, 1		#number[j] << 1
	addu	$s5, $s5, $s1		#number[j] += carrey
	sb		$s5, 0($s4)
IfSll:
	beq		$s6, $zero, EndIfSll	#If this generated any carry then put carry <- 1
	li		$s1, 1
EndIfSll:
	addi		$s2, $s2, 1		#j++
	j		While2Sll
EndWhile2Sll:
	addi		$s0, $s0, 1		#i++
	j		While1Sll
EndWhile1Sll:
	not		$a1, $a1
	addi		$a1, $a1, 1
	jal		addToExponent128
	lw		$ra, 32($sp)
	lw		$s7, 28($sp)
	lw		$s6, 24($sp)
	lw		$s5, 20($sp)
	lw		$s4, 16($sp)
	lw		$s3, 12($sp)
	lw		$s2, 8($sp)
	lw		$s1, 4($sp)
	lw		$s0, 0($sp)
	add		$sp, $sp, 36
	jr		$ra
########################################################
#void:srl128(*floatingPointNumber, numTimes)
########################################################
srl128:
	add		$sp, $sp, -32
	sw		$s0, 0($sp)
	sw		$s1, 4($sp)
	sw		$s2, 8($sp)
	sw		$s4, 12($sp)
	sw		$s5, 16($sp)
	sw		$s6, 20($sp)
	sw		$s7, 24($sp)
	sw		$ra, 28($sp)
	and		$s0, $s0, $zero	#$s0(i) <- 0
	la		$s7, maskSrl
	lw		$s7, 0($s7)		#$s7(mask) <- 0x00000001
While1Srl:
	beq		$s0, $a1, EndWhile1Srl
	and		$s1, $s1, $zero	#$s1(carrey) <- 0
	li		$s2, 13			#$s2(j) <- 13
While2Srl:
	blt		$s2, $zero, EndWhile2Srl
	add		$s4, $s2, $a0
	lbu		$s5, 0($s4)		#$s5 <- number[j]
	and		$s6, $s5,$s7		#tmpCarry <- number[j] & mask
	srl		$s5, $s5, 1		#number[j] >> 1
	addu	$s5, $s5, $s1		#number[j] += carrey
	sb		$s5, 0($s4)
	move	$s1, $s6			#carry <- tmpCarry
	addi		$s2, $s2, -1		#j--
	j		While2Srl
EndWhile2Srl:
	addi		$s0, $s0, 1		#i++
	j		While1Srl
EndWhile1Srl:
	jal		addToExponent128
	lw		$ra, 28($sp)
	lw		$s7, 24($sp)
	lw		$s6, 20($sp)
	lw		$s5, 16($sp)
	lw		$s4, 12($sp)
	lw		$s2, 8($sp)
	lw		$s1, 4($sp)
	lw		$s0, 0($sp)
	add		$sp, $sp, 32
	jr		$ra
########################################################
#int:compareExponent128(*floatingPointNumber1, *floatingPointNumber2)
########################################################
compareExponent128:			#Returns 0 if both exponents are equal, a negative number if the first exponent is less than the second one and a positive number if the first exponent is greater than the second one
	add		$sp, $sp, -12
	sw		$s0, 0($sp)
	sw		$s1, 4($sp)
	sw		$s7, 8($sp)
	lw		$s0, 12($a0)
	lw		$s1, 12($a1)
	la		$s7, maskExponent
	lw		$s7, 0($s7)
	and		$s0, $s0, $s7		#Removing the sign and mantissa
	and		$s1, $s1, $s7		#Removing the sign and mantissa
	srl		$s0, $s0, 16
	srl		$s1, $s1, 16
	subu		$v0, $s0, $s1
	lw		$s7, 8($sp)
	lw		$s1, 4($sp)
	lw		$s0, 0($sp)
	add		$sp, $sp, 12
	jr		$ra
########################################################
#int:compareMantissa128(*floatingPointNumber1, *floatingPointNumber2)
########################################################
compareMantissa128:
	addi		$sp, $sp, -16
	sw		$s0, 0($sp)
	sw		$s1, 4($sp)
	sw		$s2, 8($sp)
	sw		$s3, 12($sp)
	li		$s0, 13
WhileCompareMantissa:
	blt		$s0, $zero, EndWhileCompareMantissa
	add		$s1, $s0, $a0
	lbu		$s2, 0($s1)
	add		$s1, $s0, $a1
	lbu		$s3, 0($s1)
	sgtu		$v0, $s2, $s3
	bne		$v0, $zero, returnCompareMantissa
	sltu		$v0, $s2, $s3
	bne		$v0, $zero, SetLessCompareMantissa
	addi		$s0, $s0, -1
	j		WhileCompareMantissa
EndWhileCompareMantissa:
	and		$v0, $v0, $zero
	j		returnCompareMantissa
SetLessCompareMantissa:
	li		$v0, -1
returnCompareMantissa:
	lw		$s3, 12($sp)
	lw		$s2, 8($sp)
	lw		$s1, 4($sp)
	lw		$s0, 0($sp)
	addi		$sp, $sp, 16
	jr		$ra
########################################################
#int:compareUnsigned128(*floatingPointNumber1, *floatingPointNumber2)
########################################################
compareUnsigned128:
	addi		$sp, $sp, -4
	sw		$ra, 0($sp)
	jal		compareExponent128
	blt		$v0, $zero, SetLessCompareUnsigned
	bgt		$v0, $zero, SetGreaterCompareUnsigned
	jal		compareMantissa128
	j		returnCompareUnsigned
SetLessCompareUnsigned:
	li		$v0, -1
	j		returnCompareUnsigned
SetGreaterCompareUnsigned:
	li		$v0, 1
returnCompareUnsigned:
	lw		$ra, 0($sp)
	addi		$sp, $sp, 4
	jr		$ra
########################################################
#int:compareSign128(*floatingPointNumber1, *floatingPointNumber2)
########################################################
compareSign128:
	addi		$sp, $sp, -12
	sw		$s0, 0($sp)
	sw		$s1, 4($sp)
	sw		$s7, 8($sp)
	la		$s7, maskSign
	lw		$s7, 0($s7)
	lw		$s0, 12($a0)
	and		$s0, $s0, $s7
	lw		$s1, 12($a1)
	and		$s1, $s1, $s7
	sgtu		$v0, $s0, $s1
	bne		$v0, $zero, SetLessCompareSign
	sltu		$v0, $s0, $s1
	j		returnCompareSign
SetLessCompareSign:
	li		$v0, -1
returnCompareSign:
	lw		$s7, 8($sp)
	lw		$s1, 	4($sp)
	lw		$s0, 	0($sp)
	addi		$sp, $sp, 12
	jr		$ra
########################################################
#int:cmpFP128(*floatingPointNumber1, *floatingPointNumber2)
########################################################
cmpFP128:
	addi		$sp, $sp, -4
	sw		$ra, 0($sp)
	jal		compareSign128
	bne		$v0, $zero, returnCmpFP128
	jal		compareUnsigned128
returnCmpFP128:
	lw		$ra, 0($sp)
	addi		$sp, $sp, 4
	jr		$ra
########################################################
#int:sumarMantissas128(*floatingPointNumber1, *floatingPointNumber2, *result)
########################################################
sumarMantissas128:				#Makes the addition of the two mantissas and returns the carry generated
	addi		$sp, $sp, -24
	sw		$s0, 0($sp)
	sw		$s1, 4($sp)
	sw		$s2, 8($sp)
	sw		$s3, 12($sp)
	sw		$s4, 16($sp)
	sw		$s5, 20($sp)
	and		$s0, $s0, $zero	#Initialize $s0 in 13 (i <- 0)
	and		$s1, $s1, $zero	#Initialize $s1 in 0 (carry <- 0)
WhileSumarMantissas:
	li		$s2, 14
	beq		$s0, $s2, EndWhileSumarMantissas
	add		$s2, $s0, $a0
	lbu		$s3, 0($s2)		#$s3 <- number1[i]
	add		$s2, $s0, $a1
	lbu		$s4, 0($s2)		#$s4 <- number2[i]
	addu	$s5, $s3, $s4		#$s5 <- number1[i] + number2[i]
	addu	$s5, $s5, $s1		#result[i] += carry
	add		$s2, $s0, $a2
	sb		$s5, 0($s2)
	srl		$s1, $s5, 8		#Save the new carry
	addi		$s0, $s0, 1		#i++
	j		WhileSumarMantissas
EndWhileSumarMantissas:
	move	$v0, $s1
	lw		$s5, 20($sp)
	lw		$s4, 16($sp)
	lw		$s3, 12($sp)
	lw		$s2, 8($sp)
	lw		$s1, 4($sp)
	lw		$s0, 0($sp)
	addi		$sp, $sp, 24
	jr		$ra
########################################################
#void:normalize128(*floatingPointNumber)
########################################################
normalize128:
	addi		$sp, $sp, -16
	sw		$s0, 0($sp)
	sw		$s1, 4($sp)
	sw		$s7, 8($sp)
	sw		$ra, 12($sp)
	li		$s1, 113			#Initialize i <- 14
	la		$s7, maskSll
	lw		$s7, 0($s7)		#$s7(mask) <- 0x00000080
	li		$a1, 1
WhileNormalize:
	lbu		$s0, 13($a0)
	and		$s0, $s0, $s7
	beq		$s0, $s7, EndWhileNormalize
	beq		$s1, $zero, EndWhileNormalize
	jal		sll128
	addi		$s1, $s1, -1		#i--
	j		WhileNormalize
EndWhileNormalize:
	jal		sll128
	lw		$ra, 12($sp)
	lw		$s7, 8($sp)
	lw		$s1, 4($sp)
	lw		$s0, 0($sp)
	addi		$sp, $sp, 16
	jr		$ra
########################################################
#void:copyFP128(*floatingPointNumber, *result)
########################################################
copyFP128:
	addi		$sp, $sp, -12
	sw		$s0, 0($sp)
	sw		$s1, 4($sp)
	sw		$s2, 8($sp)
	li		$s0, 3
WhileCopy:
	blt		$s0, $zero, EndWhileCopy
	sll		$s1, $s0, 2
	add		$s1, $s1, $a0
	lw		$s2, 0($s1)
	sll		$s1, $s0, 2
	add		$s1, $s1, $a1
	sw		$s2, 0($s1)
	addi		$s0, $s0, -1
	j		WhileCopy
EndWhileCopy:
	lw		$s2, 8($sp)
	lw		$s1, 4($sp)
	lw		$s0, 0($sp)
	addi		$sp, $sp, 12
	jr		$ra
########################################################
#void:sumarFP128(*floatingPointNumber1, *floatingPointNumber2, *result)
########################################################
sumarFP128:
	addi		$sp, $sp, -28
	sw		$s0, 0($sp)
	sw		$s1, 4($sp)
	sw		$s2, 8($sp)
	sw		$s3, 12($sp)
	sw		$s4, 16($sp)
	sw		$s5, 20($sp)
	sw		$ra, 24($sp)
	jal		compareExponent128
	move	$s3, $v0
If1Sumar:
	blt		$s3, $zero, ElseIf1Sumar
	move	$s0, $a1			#Move to $s0 the smaller number
	move	$s1, $a0
	j		EndIf1Sumar
ElseIf1Sumar:
	move	$s0, $a0
	move	$s1, $a1
	not		$s3, $s3
	addi		$s3, $s3, 1
EndIf1Sumar:
#Check if the maximum number has the maximum exponent (infinite or NaN)
	la		$s4, infinite
	move	$a0, $s4
	move	$a1, $s1
	jal		compareExponent128
IfMaxExpSumar:
	bne		$v0, $zero, EndIfMaxExpSumar
	move	$a0, $s4
	move	$a1, $s0
	jal		compareUnsigned128
IfMaxExp1Sumar:
	bge		$v0, $zero, ElseIfMaxExp1Sumar	#If the first number is NaN copy it to the result
	move	$a0, $s0
	j		EndIfMaxExp1Sumar
ElseIfMaxExp1Sumar:
IfInf1Sumar:
	move	$a0, $s4
	move	$a1, $s0
	jal		compareUnsigned128
	bne		$v0, $zero, ElseIfInf1Sumar
	move	$a0, $s4
	move	$a1, $s1
	jal 		compareUnsigned128
	bne		$v0, $zero, ElseIfInf1Sumar
	move 	$a0, $s0
	move	$a1, $s1
	jal 		compareSign128
	beq		$v0, $zero, ElseIfInf1Sumar
	la		$a0, nAn
	j		EndIfMaxExp1Sumar
ElseIfInf1Sumar:
	move	$a0, $s1
EndIfMaxExp1Sumar:
	move	$a1, $a2
	jal		copyFP128
	j		EndIf3Sumar
EndIfMaxExpSumar:
If2Sumar:
	beq		$s3, $zero, EndIf2Sumar
	move	$a0, $s0
	li		$a1, 1
	jal		srl128
	lbu		$s4, 13($s0)
	la		$s5, maskSll
	lw		$s5, 0($s5)		#$s5(mask) <- 0x00000080
	or		$s4, $s4, $s5		#Add the implicit 1
	sb		$s4, 13($s0)
	move	$a1, $s3
	addi		$a1, $a1, -1
	jal		srl128
EndIf2Sumar:
	la		$s2, maskSignExponent
	lw		$s2, 0($s2)
	lw		$s4, 12($s1)
	and		$s4, $s4, $s2
	sw		$s4, 12($a2)		#Save the sign and  exponent into the result
	move	$a0, $s0
	move	$a1, $s1
	jal		sumarMantissas128
	la		$s2, maskExponent
	lw		$s2, 0($s2)
	and		$s4, $s4, $s2
	beq		$s4, $zero, EndIf3Sumar
if31Sumar:
	li		$s2, 0
	bne		$s3, $zero, If32Sumar
	li		$s2, 1
	j		If3Sumar
If32Sumar:
	beq		$v0, $zero, EndIf3Sumar
If3Sumar:
	addiu	$s2, $s2, 1
	addu	$s2, $s2, $v0
	move	$a0, $a2
	li		$a1, 2
	jal		srl128
	sll		$s2, $s2, 6
	lb		$s4, 13($a2)
	addu	$s4, $s4, $s2
	sb		$s4, 13($a2)
	move	$a0, $a2
	jal		normalize128
EndIf3Sumar:
	lw		$ra, 24($sp)
	lw		$s5, 20($sp)
	lw		$s4, 16($sp)
	lw		$s3, 12($sp)
	lw		$s2, 8($sp)
	lw		$s1, 4($sp)
	lw		$s0, 0($sp)
	addi		$sp, $sp, 28
	jr		$ra
